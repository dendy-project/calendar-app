import {Component, OnInit} from '@angular/core';
import {CalendarService} from "./services/calendar.service";

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit  {
    dates: any[] = [];
    title = 'Calendar App Payever';
    desc = 'Calendar With Drag and Drop';

    constructor(private date: CalendarService) {}

    ngOnInit() {
        this.initCalendarDates();
    }

    private initCalendarDates() {
        const { year, monthIndex } = this.date.getCurrent();

        if (monthIndex === 0) {
            // first month January
            this.dates = [
                new Date(year, monthIndex),
            ];

            return;
        }

        if (monthIndex === 11) {
            // last month December
            this.dates = [
                new Date(year, monthIndex),
            ];

            return;
        }

        this.dates = [
            new Date(year, monthIndex),
        ];
    }
}

