import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalendarRoutingModule } from './calendar-routing.module';
import { CalendarComponent } from "./calendar.component";
import { CalendarViewComponent } from './calendar-view/calendar-view.component';
import {DragDropModule} from "@angular/cdk/drag-drop";
import {CalendarService} from "./services/calendar.service";
import {MatTableModule} from "@angular/material/table";


@NgModule({
  declarations: [
    CalendarComponent,
    CalendarViewComponent
  ],
    imports: [
        CommonModule,
        CalendarRoutingModule,
        DragDropModule,
        MatTableModule,
    ],
  providers: [
    CalendarService
  ]
})
export class CalendarModule { }
