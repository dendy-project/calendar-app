import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CalendarService {

    constructor() {
    }

    getCurrent(): { year: number; monthIndex: number } {
        const currentDate = new Date();

        return {
            year: currentDate.getFullYear(),
            monthIndex: currentDate.getMonth()
        };
    }
}
